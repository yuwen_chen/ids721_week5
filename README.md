# IDS721_week5 Mini Project

## Step

### Create the table in AWS DynamoDB
Add attributes [Artist] and [SongTitle]
Add three items:
[Artist1]: [Song1]
[Artist2]: [Song2]
[Artist3]: [Song3]

### Set up Rust
Write the song retriving function in the ./main/src

### Run cargo lambda watch
### Run cargo lambda invoke --data-ascii "{ \"artist\": \"Artist2\" }

### Result
![image](image_1.png)
![image](image_2.png)
![image](image_3.png)

